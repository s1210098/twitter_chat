class RoomsController < ApplicationController
  def show
    if entered?
      @message = Message.new
      @messages = Message.all
    else
      flash[:danger] = 'Please create user'
      redirect_to root_path
    end
  end

  def create
    @message = current_user.messages.build(params_message)
    if @message.save
      RoomChannel.broadcast_to 'room_channel', message: render_message(@message)
    else
      puts 'message_errors'
      puts @message.errors.full_messages
    end
  end

  def create_twitter

  end

  private
    def params_message
      params.require(:message).permit(:content, :picture)
    end

    def render_message(message)
      ApplicationController.renderer.render(partial: 'messages/message',
                      locals: { message: message })
    end
end
