class SessionsController < ApplicationController

  def new

  end

  def create
    current_user.uid = request.env['omniauth.auth'][:uid]
    current_user.nickname = request.env['omniauth.auth'][:info][:nickname]
    current_user.save
    enter current_user
    redirect_to room_path
  end

  def destroy
    reset_session
    redirect_to root_path
  end
end
