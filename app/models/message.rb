class Message < ApplicationRecord
  belongs_to :user
  mount_uploader :picture, PictureUploader
  # after_create_commit { MessageBroadcastJob.perform_later self }
  validates :content, presence: true
  validate :picture_size

  private

    def picture_size
      if picture.size > 5.megabytes
        errors.add :picture, 'should be less than 5MB'
      end
    end
end

