onPageLoad 'users#new', ->
  pushProfileImage()
  changeProfileImage()

pushProfileImage = ->
  $('#user_img').on 'click', ->
    $('#user_image').trigger('click')

changeProfileImage = ->
  $('input[type=file]').change ->
    file = $(this).prop('files')[0]
    reader = new FileReader()
    reader.onload = ->
      $('#user_img').css 'background-image', "url(" + reader.result + ")"
    reader.readAsDataURL(file)
