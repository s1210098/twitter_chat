App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $(data['message']).appendTo('#messages')
    refreshForm()
    checkFile()
    scroll()

#  speak: (message) ->
#    @perform 'speak', message: message

  speak: (message) ->
    @perform 'speak'

$(document).ready ->
  sendTwitter()

checkFile = ->
  if $('#message_picture')[0].files.length == 0
    $('#image_button').popover('destroy')

scroll = ->
  $('.middle').animate({scrollTop: $('.middle')[0].scrollHeight}, 'fast')

sendTwitter = ->
  $('#send_twitter').click ->
    App.room.speak ''

refreshForm = ->
  $('#message_picture').val ''
  $('#message_content').val ''
