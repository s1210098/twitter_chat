onPageLoad 'rooms#show', ->
  pushInput()
  checkFileSize()
  checkFile()
  chooseFile()
  scroll()

pushInput = ->
  $('#image_button').click ->
    $('#message_picture').trigger('click')

checkFileSize = ->
  $('#message_picture').bind 'change', ->
    size_in_megabytes = this.files[0].size/1024/1024
    if size_in_megabytes > 5
      alert 'Maximum file size in 5MB. Please choose a smaller file.'

chooseFile = ->
  $('input[type=file]').on 'change', ->
    $('#image_button').attr('data-content', $('#message_picture')[0].files[0].name)
    $('#image_button').popover('show')
    scroll()

checkFile = ->
  if $('#message_picture')[0].files.length == 0
    $('#image_button').popover('destroy')

scroll = ->
  $('.middle').animate({scrollTop: $('.middle')[0].scrollHeight}, 'fast')
