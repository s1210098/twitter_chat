module RoomsHelper
  def get_profile_image(message)
    get_twitter_data(message)[0]
  end

  def get_profile_name(message)
    get_twitter_data(message)[1]
  end

  def get_profile_nick_name(message)
    get_twitter_data(message)[2]
  end

  def get_profile_description(message)
    get_twitter_data(message)[3]
  end

  def get_profile_url(message)
    get_twitter_data(message)[4]
  end

  def get_twitter_data(message)
    data = []
    message.lines { |line| data.push line }
    data
  end
end
