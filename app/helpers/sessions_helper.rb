module SessionsHelper
  def enter(user)
    session[:user_id] = user.id
    session[:u_id] = user.uid unless user.uid.nil?
  end

  def entered?
    !session[:user_id].nil?
  end

  def leave
    session.delete :user_id
  end

  def current_user
    if user_id = session[:user_id]
      @current_user ||= User.find_by(id: user_id)
    end
  end

  def logged_in?
    !!session[:user_id]
  end

  def logged_in_with_twitter?
    !!session[:u_id]
  end

  def authenticate
    return if logged_in?
    redirect_to root_path, alert: 'ログインしてください'
  end
end
