class RoomChannel < ApplicationCable::Channel

  def subscribed
    stream_for 'room_channel'
    stream_from 'room_channel'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key = ENV['TWITTER_KEY']
      config.consumer_secret = ENV['TWITTER_SECRET']
      config.access_token = ENV['TWITTER_TOKEN']
      config.access_token_secret = ENV['TWITTER_TOKEN_SECRET']
    end

    # twitter_user = client.user current_user.uid
    debugger
    twitter_user = client.user current_user.nickname
    debugger


    message = current_user.messages.new content: twitter_content(twitter_user),
                                        twitter: true
    message.save
    renderer = ApplicationController.renderer.render partial: 'messages/message',
                                                     locals: { message: message }
    ActionCable.server.broadcast 'room_channel', message: renderer
  end

  private
    def twitter_content(twitter_user)
      twitter_url = 'https://twitter.com'

      twitter_user.profile_image_url.to_s + "\n" +
      twitter_user.name + "\n" +
      "@#{twitter_user.screen_name}" + "\n" +
      twitter_user.description + "\n" +
      twitter_url + "/#{twitter_user.screen_name}"
    end

end
