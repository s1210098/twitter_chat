# README

自由課題で作成した簡易チャットアプリです。ローカルでこのアプリを動かすには以下の手順が必要です。

- direnvの設定  

```
~/.zshrc

export EDITOR=お使いのエディタ
eval "$(direnv hook zsh)"
```

```
cd sample_chat/
```

```
direnv edit .
```

```
.envrc

export TWITTER_KEY = ツイッターのCONSUMER_KEY
export TWITTER_SECRET = ツイッターのCONSUMER_SECRET_KEY
export TWITTER_TOKEN = ツイッターのACCESS_TOKEN
export TWITTER_TOKEN_SECRET = ツイッターのACESS_TOKEN_SECRET
```


以下参照
http://qiita.com/kompiro/items/5fc46089247a56243a62