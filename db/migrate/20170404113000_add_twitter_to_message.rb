class AddTwitterToMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :twitter, :boolean, default: false, null: false
  end
end
